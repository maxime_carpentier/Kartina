import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/core';

import { Oeuvre } from '../classes/Oeuvre';
import { Format } from '../enum/Format';
import { Finition } from '../enum/Finition';
import { Cadre } from '../enum/Cadre';
import { Artiste } from '../classes/Artiste';
import { Orientation } from '../enum/Orientation';
import { TNodeFlags } from '@angular/core/src/render3/interfaces/node';
import { OeuvresService } from '../services/oeuvres.service';

@Component({
  selector: 'app-achat',
  templateUrl: './achat.component.html',
  styleUrls: ['./achat.component.scss']
})
export class AchatComponent implements OnInit {	

  private format : boolean = true;
  private finition : boolean = false;
  private cadre : boolean = false;

  ariane1 : string = "Photographies";
  ariane2 : string = "Nom de la photo";
  linkAriane1 : string = "/photographies";
  
  private artiste: any;
  private artistePrenom: any;
  private artisteNom: any;
  private artisteId: any;
  private bio: any;
  private id: any;
  //parent->enfant
  private prix: any;
  private titre: any
  private photo : any;
  private orientation:any;  
  
  //enfant->parent
  private callOnFinition: any;
  private callRecupFormat: any;
  
  
  
  private formatData: any;
  private finitionData: any;
  private cadreData: any;
  
  private prixInt1: number;
  private prixInt2: number;
  
  private prixFinal: number;
  
  private valeurCookie: any;

  tableFormatsDispo : any;
  
  
  private carac:Array<any> = new Array("format", "finition");
  
  oeuvresPut: Array<Oeuvre> = new Array<Oeuvre>();
  oeuvresGet: any;
  
  
  
  //private caracTest:Array<any> = new Array("format", "finition", "cadre");
  
  constructor(private oeuvresService : OeuvresService, private _router: Router, private _route: ActivatedRoute, private http: HttpClient, public cookieService: CookieService) {
	this.carac.push(Cadre.SANS_ENCADREMENT);
    let id_oeuvre : number = parseInt(this._route.snapshot.paramMap.get('idPhoto'));
    console.log(id_oeuvre);    
    this.http.post("http://localhost:8080/Kartina/AcheterPhoto", id_oeuvre).subscribe(data =>{this.retour();    	
    });    
    
     console.log(this.prix);
     
     //this.carac[0] = "geant";
	console.log("carac1", this.carac);

  }

  ngOnInit() {
  }
  
  retour()
  {
  this.http.get("http://localhost:8080/Kartina/AcheterPhoto").subscribe(data =>{
   		 //this.photo = data;
			console.log(data);
			//console.log("longueurdata", data.toString.length);
		
   		 this.artistePrenom = data[0].artiste.prenom;
			this.artisteNom = data[0].artiste.nom;
			this.artisteId = data[0].artiste.id;
   		 
   		 this.artiste = data[0].artiste.prenom + " " + data[0].artiste.nom;
   		 this.bio = data[0].artiste.biographie;
   		 this.id = data[0].id;
			this.prix = data[0].prixDeBase;
			console.log(this.prix);
   		 this.titre = data[0].titrePhoto;
			this.photo = data[0].lienImage;
			this.orientation = data[0].orientation;
			//console.log("formats photo", this.listeFormat);
			let prix1 = this.prix;
			this.oeuvresService.getFormatsParOeuvre(this.id).subscribe(data => {
				this.tableFormatsDispo = data;
				console.log(this.tableFormatsDispo);
			  });
   		 });   		 
   	}

  onFormat(){
    this.format = true;
    this.finition = false;
    this.cadre = false;
  }
  
  recupFormat(event: string)
  {
  	this.carac[0] = event;
	console.log("carac2", this.carac);
	switch(this.carac[0])
	{
		case 0:
		{
			this.prixInt1 = this.prix * 1.3;
			console.log(this.prixInt1);
			break;
		}
		case 1:
		{
			this.prixInt1 = this.prix * 2.6;
			console.log(this.prixInt1);
			break;
		}
		case 2:
		{
			this.prixInt1 = this.prix * 5.2;
			console.log(this.prixInt1);
			break;
		}
		case 3:
		{
			this.prixInt1 = this.prix * 13;
			console.log(this.prixInt1);
			break;
		}
		default:
		{
			this.prixInt1 = this.prix;
			console.log(this.prixInt1);
			break;
		}
	}
  }

  onFinition(){
    this.format = false;
    this.finition = true;
    this.cadre = false;
    console.log("carac3", this.carac);
    console.log("carac4", this.carac[1]);
  }
  
  recupFinition(event: string)
  {
  	console.log("recupFinition");
  	this.carac[1] = event;
	console.log("carac5", this.carac);
	console.log("carac6", this.carac[1]);
	
	
	switch(this.carac[1])
	{
		case 4:
		{
			this.prixInt2 = this.prixInt1 * 1.4;
			console.log(this.prixInt2);
			break;
		}
		case 0:
		{
			this.prixInt2 = this.prixInt1 * 2.6;
			console.log(this.prixInt2);
			break;
		}
		case 1:
		{
			this.prixInt2 = this.prixInt1 * 3.35;
			console.log(this.prixInt2);
			break;
		}
		case 2:
		{
			this.prixFinal = this.prixInt1;
			console.log(this.prixFinal);
			break;
		}
		default:
		{
			this.prixInt2 = this.prixInt1;
			console.log(this.prixInt2);
			break;
		}
	}
  }

  onCadre(){
    this.format = false;
    this.finition = false;
    this.cadre = true;
    console.log("carac7", this.carac);
  }
  
  recupCadre(event: string)
  {
  	console.log("recupCadre");
  	this.carac[2] = event;
	console.log("carac8", this.carac);
	console.log("carac9", this.carac[2]);
	
	switch(this.carac[2])
	{
		case 1:
		{
			this.prixFinal = this.prixInt2 * 1.45;
			console.log(this.prixFinal);
			break;
		}
		case 2:
		{
			this.prixFinal = this.prixInt2 * 1.45;
			console.log(this.prixFinal);
			break;
		}
		case 3:
		{
			this.prixFinal = this.prixInt2 * 1.45;
			console.log(this.prixFinal);
			break;
		}
		case 4:
		{
			this.prixFinal = this.prixInt2 * 1.45;
			console.log(this.prixFinal);
			break;
		}
		default:
		{
			this.prixFinal = this.prixInt2;
			console.log(this.prixFinal);
			break;
		}
	}
  }
  
  envoiCookie()
  {
  	//this.cookieService.putObject("oeuvres", null);  	
  	//console.log("test0", this.cookieService.getObject("oeuvres"));
	
	  this.oeuvresGet = this.cookieService.getObject("oeuvres");
  	//let i:number = this.oeuvresPut.length;
  	
  	
  	
  	let oeuvre: Oeuvre = new Oeuvre();
    oeuvre.id = this.id;
    oeuvre.formatChoisi = this.carac[0];
    oeuvre.finition = this.carac[1];
    oeuvre.cadre = this.carac[2];
    oeuvre.lienImage = this.photo;
    oeuvre.titrePhoto = this.titre;
    let artiste: Artiste = new Artiste();
    artiste.id = this.artisteId;
    artiste.nom = this.artisteNom;
    artiste.prenom = this.artistePrenom;
    oeuvre.artiste = artiste;
    oeuvre.prixAchat = this.prixFinal;
    oeuvre.quantite = 1;
    oeuvre.orientation = this.orientation;
    
    this.oeuvresGet.push(oeuvre);
   
	this.cookieService.putObject("oeuvres", this.oeuvresGet);
	
	window.location.reload();
    console.log("test1", this.cookieService.getObject("oeuvres"));
    
  

  
  
  
  
  
  
  
  
  
  
  /*
  
  	this.valeurCookie = "{\"id\":" + this.id + 
  						",\"format\":\"" + this.carac[0] +
  						"\",\"finition\":\"" + this.carac[1] +
  						"\",\"cadre\":\"" + this.carac[2] +
  						"\",\"lienImage\":\"" + this.photo +
  						"\",\"titre\":\"" + this.titre +
  						"\",\"artiste\":\"" + this.artiste +
  						"\",\"prix\":" + this.prixFinal +
  						",\"quantite\":1" + "}";
  	console.log("valeur cookie : ", this.valeurCookie);
  	
  	this.cookieService.put("oeuvre", this.valeurCookie);
  				*/		
  }

  routingToArtiste(id: number) {
	this._router.navigate(['/artiste', id]);
}
  

  ngAfterViewInit(){
    window.scroll(0,0);
  }

}
