import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Oeuvre } from '../classes/Oeuvre';
import { CookieOptions } from 'angular2-cookie/services/base-cookie-options';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Format } from '../enum/Format';
import { Finition } from '../enum/Finition';
import { Cadre } from '../enum/Cadre';
import { Artiste } from '../classes/Artiste';
import { Orientation } from '../enum/Orientation';
import { Router } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { Commande } from '../classes/Commande';
import { Client } from '../classes/Client';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  ariane1: string = "Panier";
  //oeuvresPut: Array<Oeuvre> = new Array<Oeuvre>();
  oeuvresGet: any;
  prixTotal : number = 0;
  commande: Commande = new Commande();
  @Output() pastille : EventEmitter<number> = new EventEmitter<number>();
  panierRempli : boolean = false;

  constructor(private _sessionSt: SessionStorageService, private cookieService: CookieService, private _router : Router) {

    // Côté parcours d'achat
    /*let oeuvre1: Oeuvre = new Oeuvre();
    oeuvre1.id = 7;
    oeuvre1.formatChoisi = Format.COLLECTOR;
    oeuvre1.finition = Finition.ALUMINIUM;
    oeuvre1.cadre = Cadre.NOYER;
    oeuvre1.lienImage = "chevaux.jpg";
    oeuvre1.titrePhoto = "Chevaux";
    let artiste1: Artiste = new Artiste();
    artiste1.id = 6;
    artiste1.nom = "Hadet";
    artiste1.prenom = "Nolwenn";
    oeuvre1.artiste = artiste1;
    oeuvre1.prixAchat = 78 * 13 * 2.6 * 1.45;
    oeuvre1.quantite = 1;
    oeuvre1.orientation = Orientation.PAYSAGE;
    this.oeuvresPut.push(oeuvre1);

    let oeuvre2: Oeuvre = new Oeuvre();
    oeuvre2.id = 13;
    oeuvre2.formatChoisi = Format.GEANT;
    oeuvre2.finition = Finition.VERRE_ACRYLIQUE;
    oeuvre2.cadre = Cadre.NOIR_SATIN;
    oeuvre2.lienImage = "elephant.jpg";
    oeuvre2.titrePhoto = "Elephant";
    let artiste2: Artiste = new Artiste();
    artiste2.id = 5;
    artiste2.nom = "Dequick";
    artiste2.prenom = "Laurent";
    oeuvre2.artiste = artiste2;
    oeuvre2.prixAchat = 48 * 5.2 * 3.35 * 1.45;
    oeuvre2.quantite = 1;
    oeuvre2.orientation = Orientation.CARRE;
    this.oeuvresPut.push(oeuvre2);

    cookieService.putObject("oeuvres", this.oeuvresPut);*/

    this.oeuvresGet = cookieService.getObject("oeuvres");

    this.pastille.emit(this.oeuvresGet.length);
    this.calculPrixTotal();

    if (this.oeuvresGet.length > 0){
      this.panierRempli = true;
    } else {
      this.panierRempli = false;
    }
    //console.log((cookieService.getObject("oeuvres")[0])._id);


  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    window.scroll(0, 0);
  }

  routingToAchat(id: number) {
    this._router.navigate(['/achat', id]);
  }

  routingToArtiste(id: number) {
    this._router.navigate(['/artiste', id]);
  }


  supprimerArticle(id:number){
    for(let i=0; i<this.oeuvresGet.length; i++){
      if (this.oeuvresGet[i]._id == id){
        this.oeuvresGet.splice(i,1);
      }
    }
    this.cookieService.putObject("oeuvres", this.oeuvresGet);
    this.calculPrixTotal();
    this.oeuvresGet = this.cookieService.getObject("oeuvres");
    if (this.oeuvresGet.length > 0){
      this.panierRempli = true;
    } else {
      this.panierRempli = false;
    }
    this.pastille.emit(this.oeuvresGet.length);
    window.location.reload();
  }

  calculPrixTotal(){
    this.prixTotal = 0;
    for(let i=0; i<this.oeuvresGet.length; i++){
      this.prixTotal += (this.oeuvresGet[i]._prixAchat * this.oeuvresGet[i]._quantite);
      this.cookieService.putObject("oeuvres",this.oeuvresGet);
    }
  }

  onPaiement(){
    let client : Client = new Client();
    client.id = 2;
    this.commande.client = client;
    this.commande.oeuvre = this.oeuvresGet;
    this.commande.prix = this.prixTotal;
    this.cookieService.putObject("commande", this.commande);
    if (this._sessionSt.retrieve("user")){
      this._router.navigateByUrl('/paiement');
    } else {
      this._router.navigateByUrl('/compte');
    }
  }

}
