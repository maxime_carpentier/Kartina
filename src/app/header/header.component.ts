import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { UtilisateursService } from '../services/utilisateurs.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  compte: string = "Compte";
  user: any;
  decon : boolean = false;
  pastille : number = 0;
  oeuvresGet: any;

  constructor(cookieService : CookieService, private _route: Router, private _utilisateursService: UtilisateursService, private _sessionSt: SessionStorageService) { 
    this.oeuvresGet = cookieService.getObject("oeuvres");
  }

  ngOnInit() {
    if (this._sessionSt.retrieve("user")) {
      this._utilisateursService.getUtilisateurSession(this._sessionSt.retrieve("user").id).subscribe(data => {
        this.user = data;
        this.compte = this.user.prenom;
        this.decon = true;
      });
    } else {
      this.compte = "Compte";
      this.decon = false;
    }
  }

  ngAfterContentInit(){
    this.pastille = this.oeuvresGet.length;
  }

  lienCompte() {
    if (this._sessionSt.retrieve("user")) {
      Promise.resolve().then(()=> {
        this.ngOnInit();
      })
      .then(() => {
        this._route.navigateByUrl("user");
      });
    } else {
      Promise.resolve().then(()=> {
        this.ngOnInit();
      })
      .then(() => {
        this._route.navigateByUrl("compte");
      });
    }
  }

  deconnexion() {
    Promise.resolve().then(()=> {
      this._sessionSt.clear("user");
    })
    .then(() => {
      this.ngOnInit();
    })
    .then(() => {
      this._route.navigateByUrl("compte");
    });
  }


}
