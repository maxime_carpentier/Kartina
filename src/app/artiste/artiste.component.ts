import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Artiste } from '../classes/Artiste';
import { ArtistesService } from '../services/artistes.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-artiste',
  templateUrl: './artiste.component.html',
  styleUrls: ['./artiste.component.scss']
})
export class ArtisteComponent implements OnInit {

  ariane1: string = "Artistes";
  ariane2: string = "Nom de l'artiste";
  linkAriane1: string = "/artistes";

  id_artiste: number;
  artiste: Artiste;

  artisteRecup: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private _artistesService: ArtistesService, public http: HttpClient) {

    this.id_artiste = parseInt(this._route.snapshot.paramMap.get('idArtiste'));

    this._artistesService.getArtiste(this.id_artiste).subscribe(data => {
      this.artisteRecup = data;
      this.artiste = this.artisteRecup;
      this.ariane2 = this.artiste.prenom + " " + this.artiste.nom
    });

  }

  ngOnInit() {

  }

  routingToAchat(id: number) {
    this._router.navigate(['/achat', id]);
  }

  routingToArtiste(id: number) {
    this._router.navigate(['/artiste', id]);
  }



}
