import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-filariane',
  templateUrl: './filariane.component.html',
  styleUrls: ['./filariane.component.scss']
})
export class FilarianeComponent implements OnInit {

  @Input() ariane1 : string;
  @Input() ariane2 : string;
  @Input() boolAriane2 : boolean = false;
  @Input() linkAriane1 : string;

  constructor() { }

  ngOnInit() {
    if (!this.boolAriane2){
      document.getElementById("fil_ariane_2").addEventListener("mouseover", function(){
        document.getElementById("fil_ariane_2").style.color =  "#544a3f";
        document.getElementById("fil_ariane_2").style.cursor =  "auto";
        document.getElementById("fil_ariane_2").style.fontSize =  "13px";
      });
    }
  }

}
