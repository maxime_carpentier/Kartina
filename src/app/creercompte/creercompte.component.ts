import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilisateursService } from '../services/utilisateurs.service';



@Component({
	selector: 'app-creercompte',
	templateUrl: './creercompte.component.html',
	styleUrls: ['./creercompte.component.scss']
})
export class CreercompteComponent implements OnInit {

	ariane1: string = "Mon compte";

	private formValide: boolean = false;

	_json: string = "{";

	_civilite: string = "";
	_nom: string = "";
	_prenom: string = "";
	_email: string = "";
	_mot_de_passe: string = "";
	_check_mot_de_passe: string = "";
	_telephone: string = "";
	_rue: string = "";
	_codePostal: string = "";
	_ville: string = "";
	_pays: string = "";

	confirmation: any;
	checkPwd: boolean = false;
	checkEmail: any;
	checkEmailBool: boolean;



	constructor(public http: HttpClient, private _utilisateursService: UtilisateursService) { }

	ngOnInit() {
	}

	verifEmail() {
		this._utilisateursService.getCheckEmailInscription(this._email).subscribe(data => {
			this.checkEmail = data;
			this.checkEmailBool = this.checkEmail;
			console.log(this.checkEmailBool);
		});
	}

	creation() {
		this._json = "{";
		this.checkPwd = true;
		this.formValide = true;
		this.confirmation = false;
		this._json += "\"civilite\":\"" + this._civilite + "\"," +
			"\"prenom\":\"" + this._prenom + "\"," +
			"\"nom\":\"" + this._nom + "\"," +
			"\"email\":\"" + this._email + "\"," +
			"\"motDePasse\":\"" + this._mot_de_passe + "\"," +
			"\"telephone\":\"" + this._telephone + "\"," +
			"\"rue\":\"" + this._rue + "\"," +
			"\"codePostal\":\"" + this._codePostal + "\"," +
			"\"ville\":\"" + this._ville + "\"," +
			"\"pays\":\"" + this._pays + "\"" + "}";

		if ((this._mot_de_passe != this._check_mot_de_passe) || this._mot_de_passe == "") {
			this.checkPwd = false;
		}

		if ((this._civilite == "") || (this._prenom == "") || (this._nom == "") || (this._email == "") ||
			(this._mot_de_passe == "") || (this._telephone == "") || (this._rue == "") || (this._codePostal == "") ||
			(this._ville == "") || (this._pays == "")) {
			this.formValide = false;
		}

		if (this.checkEmailBool && this.checkPwd && this.formValide) {
			this.confirmation = true;
			this._utilisateursService.postInscriptionUtilisateur(this._json);
		}



		/*console.log(this._json);
		if ((this._civilite != "") && (this._prenom != "") && (this._nom != "") && (this._email != "") &&
			(this._mot_de_passe != "") && (this._telephone != "") && (this._rue != "") && (this._codePostal != "") &&
			(this._ville != "") && (this._pays != "")) {
			this.formValide = true;
			var jsonData = JSON.parse(this._json);  	
			console.log(jsonData);  	
			this.http.post("http://localhost:8080/Kartina/JSONITest", this._json).subscribe();


			



			this.http.get("http://localhost:8080/Kartina/JSONITest").subscribe(data => 
			{
				//console.log("test1");
				console.log(data.status);
				this.confirmation = data.status;
				//console.log(this.confirmation);
  				console.log("test");
  				//this.confirmationData = data;
  				//this.confirmation = this.confirmationData.status;
  				//var confirmation = JSON.parse(data).status;
				//console.log("conf1 = " + this.confirmationData);

  				this.http.get("http://localhost:8080/Kartina/JSONITest?confirmation=" + this.confirmation).subscribe(data => {
  										//console.log("type : " + typeof data.status);
  										console.log("test");
  										this.confirmationData = data;
  										this.confirmation = this.confirmationData.status;
  										//var confirmation = JSON.parse(data).status;
  										



			//	});
			//		console.log(this.confirmation);

			//console.log("conf2 = " + this.confirmation);

			//		

	} 
		else {
			console.log("il y a des champs vides");
		}*/





	}



}
