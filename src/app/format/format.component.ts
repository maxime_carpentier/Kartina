import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Format } from '../enum/Format';
import { OeuvresService } from '../services/oeuvres.service';
import { HttpXsrfCookieExtractor } from '@angular/common/http/src/xsrf';

@Component({
  selector: 'app-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.scss']
})
export class FormatComponent implements OnInit, OnChanges {


  private choixFormat:boolean = false;
  private choixClassique:boolean = true;
  // private choixGrand:boolean = true;
  private choixGeant:boolean = false;
  private choixCollector:boolean = false;	

  
	
	//parent->enfant
  @Input() prixFormat:any;	
	@Input() tableFormatsDispo :any;
	//enfant->parent
	@Output() etapeFinition = new EventEmitter<boolean>();		
	@Output() formatData = new EventEmitter<Format>();

  

  constructor(private oeuvresService : OeuvresService) { }

  ngOnInit() {
  
  }

  ngOnChanges() {
  }
  

  choixgrand() {
    this.choixFormat = true;
    this.formatData.emit(Format.GRAND);
    this.surlignementChoix("grand");
    console.log("grand");
  }

  choixgeant() {
    this.choixFormat = true;
    this.formatData.emit(Format.GEANT);
    this.surlignementChoix("geant");
    console.log("geant");
  }

  choixcollector() {
    this.choixFormat = true;
    this.formatData.emit(Format.COLLECTOR);
    this.surlignementChoix("collector");
    console.log("collector");
  }

  choixclassique() {
    this.choixFormat = true;
    this.formatData.emit(Format.CLASSIQUE);
    this.surlignementChoix("classique");
    console.log("classique");
  }

  surlignementChoix(choix: string) {
    let choixPossibles: Array<string> = [];
    if(document.getElementsByClassName("grand")[0] as HTMLElement != undefined){
      choixPossibles.push("grand");
    }
    if(document.getElementsByClassName("geant")[0] as HTMLElement != undefined){
      choixPossibles.push("geant");
    }
    if(document.getElementsByClassName("collector")[0] as HTMLElement != undefined){
      choixPossibles.push("collector");
    }
    if(document.getElementsByClassName("classique")[0] as HTMLElement != undefined){
      choixPossibles.push("classique");
    }
    for (let i = 0; i < choixPossibles.length; i++) {
      (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseenter = function () {
      };
      (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseleave = function () {
      };
      if (choixPossibles[i] == choix) {
        console.log("if", choixPossibles[i]);
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#544a3f";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "white";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "none";
      } else {
        console.log("else", choixPossibles[i]);
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#f9fbff";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "#544a3f";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "2px solid #a89683";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseenter = function () {
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#544a3f";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "white";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "none";
        };
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseleave = function () {
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#f9fbff";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "#544a3f";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "2px solid #a89683";
        };
      }
    }
  }



  versFinition() {
    this.etapeFinition.emit(true);
  }





}
