import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UtilisateursService } from '../services/utilisateurs.service';
import { SessionStorageService } from 'ngx-webstorage';
import { HeaderComponent } from '../header/header.component';



@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.scss']
})
export class CompteComponent implements OnInit {

  ariane1: string = "Mon compte";

  _json: string = "{";

  _email: string;
  _mot_de_passe: string;
  status: boolean = false;

  checkConnexion: any;
  checkConnexionBool: boolean = true;
  checkCompteBloque: any;
  checkCompteBloqueBool: boolean = false;


  constructor(private _sessionSt: SessionStorageService, public http: HttpClient, public router: Router, private _utilisateursService: UtilisateursService) { }

  ngOnInit() {
  }

  verifIdentifiants() {
    this._utilisateursService.getCheckEmailEtPasswordCnnexion(this._email, this._mot_de_passe).subscribe(data => {
      this.checkConnexion = data;
      this.checkConnexionBool = this.checkConnexion;
      console.log(this.checkConnexionBool);
      if (this.checkConnexionBool) {
        this._utilisateursService.getCheckCompteBloqueConnexion(this._email).subscribe(data => {
          this.checkCompteBloque = data;
          this.checkCompteBloqueBool = this.checkCompteBloque;
          console.log(this.checkCompteBloqueBool);
        });
      }
    });
  }

  connexion() {
    this._json = "{";
    this._json += "\"email\":\"" + this._email + "\"," +
      "\"motDePasse\":\"" + this._mot_de_passe + "\"" + "}";
    console.log(this._json);

    if (!this.checkCompteBloqueBool && this.checkConnexionBool){
      this._utilisateursService.getUtilisateurViaEmail(this._email).subscribe(data => {
        this._sessionSt.store("user", data);
        this.router.navigateByUrl('/user');
        window.location.reload();
      });
    }

    // this.http.post("http://localhost:8080/Kartina/JSONCTest", this._json).subscribe();
    
  }



}
