import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(public http: HttpClient) {     

  }

  public getUsers(){
    return this.http.get("http://localhost:8080/EssaiWS/ListeClientsGson");
   
  }

  public testGet(){
    return this.http.get("http://localhost:8080/Kartina/Test");
  }

  public testPost(){
    return this.http.post("http://localhost:8080/Kartina/Test", "bonjour");
  }
  

  public postUsers(data: any){
    return this.http.post("http://localhost:8080/EssaiWS/ListeClientsGson", data);
  }

}
