import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommandesService {

  constructor(public http: HttpClient) { }

  public getCommandeViaId(id:number){
    return this.http.get("http://localhost:8080/Kartina/RecupererCommandeParId?id="+id);
  }

  public getCommandes(){
    return this.http.get("http://localhost:8080/Kartina/RecupererCommandes");
  }

  public postStatutCommande(data:any){
    return this.http.post("http://localhost:8080/Kartina/ModifierStatutCommandes", data);
  }

  public postNouvelleCommande(data:any){
    return this.http.post("http://localhost:8080/Kartina/NouvelleCommande", data);
  }

  public postNouvelleCommandesOeuvres(data:any){
    return this.http.post("http://localhost:8080/Kartina/NouvelleCommandesOeuvres", data);
  }

  public getCommandesParUtilisateur(id:number){
    return this.http.get("http://localhost:8080/Kartina/RecupererCommandesParUtilisateur?id=" + id);
  }

}
