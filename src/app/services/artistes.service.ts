import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArtistesService {

  constructor(public http: HttpClient) { }

  public getArtistes(lettre : string){
    return this.http.get("http://localhost:8080/Kartina/RecupererArtistes?lettre=" + lettre);
  }

  public getArtiste(id : number){
    return this.http.get("http://localhost:8080/Kartina/RecupererArtiste?id=" + id);
  }

}
