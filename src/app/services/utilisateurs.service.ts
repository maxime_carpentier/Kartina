import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Client} from '../classes/Client';

@Injectable({
  providedIn: 'root'
})
export class UtilisateursService {

  constructor(public http: HttpClient) {
  }

  public getUtilisateurSession(id: number) {
    return this.http.get('http://localhost:8080/Kartina/RecupererUtilisateurSession?id=' + id);
  }

  public postUpdateInfosUtilisateur(id: number, data: any) {
    return this.http.post('http://localhost:8080/Kartina/ModifierInfosUtilisateur?id=' + id, data);
  }

  public postUpdateAdresseUtilisateur(id: number, data: any) {
    return this.http.post('http://localhost:8080/Kartina/ModifierAdresseUtilisateur?id=' + id, data);
  }

  public getListeUtilisateurs() {
    return this.http.get('http://localhost:8080/Kartina/RecupererUtilisateurs');
  }

  public postUpdateActivVendeur(data: any) {
    return this.http.post('http://localhost:8080/Kartina/ModifierStatutVendeur', data);
  }

  // Pour page AIDE : appel de la ressource (servlet  pour Récupérer Infos client)
  public getUtilisateurClient() {
    return this.http.get<Client>('http://localhost:8080/Kartina/ChargerClientAide');
  }

  // Pour page AIDE : appel de la ressource (servlet  pour envoyer MAIL au client)
  public postEnvoiEmailClientAide(data: any) {
    return this.http.post('http://localhost:8080/Kartina/EnvoiEmailClientAide', data);
  }

  public getCheckEmailInscription(email: string) {
    return this.http.get('http://localhost:8080/Kartina/InscriptionVerificationEmail?email=' + email);
  }

  public postInscriptionUtilisateur(data: any) {
    return this.http.post('http://localhost:8080/Kartina/InscrireUtilisateur', data).subscribe();
  }

  public getCheckEmailEtPasswordCnnexion(email: string, password: string) {
    return this.http.get('http://localhost:8080/Kartina/VerifExistenceEmailEtMdp?email=' + email + '&password=' + password);
  }

  public getCheckCompteBloqueConnexion(email: string) {
    return this.http.get('http://localhost:8080/Kartina/VerifCompteBloqueConnexion?email=' + email);
  }

  public getUtilisateurViaEmail(email: string) {
    return this.http.get('http://localhost:8080/Kartina/RecupererUtilisateurViaEmail?email=' + email);
  }

  public postBiographie(id: number, data: any) {
    return this.http.post('http://localhost:8080/Kartina/ModifierBiographie?id=' + id, data);
  }

  public postModifierUtilisateur(id: number, data: any) {
    return this.http.post('http://localhost:8080/Kartina/ModifierUtilisateur?id=' + id, data);
  }

}
