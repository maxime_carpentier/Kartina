import { TestBed, inject } from '@angular/core/testing';

import { ArtistesService } from './artistes.service';

describe('ArtistesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArtistesService]
    });
  });

  it('should be created', inject([ArtistesService], (service: ArtistesService) => {
    expect(service).toBeTruthy();
  }));
});
