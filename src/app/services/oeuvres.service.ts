import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OeuvresService {

  constructor(public http: HttpClient) { }

  public getOeuvres(numeroPage : number, orderBy : string, ascdesc : string, derniers:string, theme:string, nouveautes:string, orientation:string, format:string, prix:string){
    return this.http.get("http://localhost:8080/Kartina/RecupererPhotos?numeroPage=" + numeroPage + "&orderBy=" + orderBy + "&ascdesc=" + ascdesc + "&derniers=" + derniers + "&theme=" + theme + "&nouveautes=" + nouveautes + "&orientation=" + orientation + "&format=" + format + "&prix=" + prix);
  }

  public getNbreOeuvres(derniers:string, nouveautes:string, theme:string, orientation:string, format:string, prix:string){
    return this.http.get("http://localhost:8080/Kartina/NbrePhotos?derniers=" + derniers + "&nouveautes=" + nouveautes + "&theme=" + theme + "&orientation=" + orientation + "&format=" + format + "&prix=" + prix);
  }

  public postNouvelleOeuvre(id_artiste:number, data:any){
    return this.http.post("http://localhost:8080/Kartina/NouvelleOeuvre?id_artiste=" + id_artiste, data).subscribe();
  }

  public postNouvelleImage(file: File){
    return this.http.post("http://localhost:8080/Kartina/Test", file).subscribe();
  }

  public getOeuvresEnCoursParArtiste(id: number){
    return this.http.get("http://localhost:8080/Kartina/RecupererVentesCourantesParArtiste?id=" + id);
  }

  public getOeuvresPasseesParArtiste(id: number){
    return this.http.get("http://localhost:8080/Kartina/RecupererVentesPasseesParArtiste?id=" + id);
  }

  public getFormatsParOeuvre(id : number){
    return this.http.get("http://localhost:8080/Kartina/RecupererFormatsParPhoto?id_oeuvre=" + id);
  }

}
