import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import { CommandesService } from '../services/commandes.service';
import { Oeuvre } from '../classes/Oeuvre';
import { Finition } from '../enum/Finition';
import { Format } from '../enum/Format';
import { Cadre } from '../enum/Cadre';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
declare var Stripe: any;

@Component({
    //moduleId: module.id,
    selector: 'app-paiement',
    templateUrl: './paiement.component.html',
    styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {

    private formValide: boolean = false;
    private accueil: boolean = false;

    @Input() amount;
    stripe: any;
    card: any;
    @Output() token = new EventEmitter<any>();

    ariane1: string = "Panier";
    ariane2: string = "Paiement";
    linkAriane1: string = "/panier";
    commande: any;
    prixData : number;

    utilisateur : any;

    constructor(private _router : Router, private commandesService : CommandesService, private utilisateursService: UtilisateursService, private _sessionSt: SessionStorageService, private cookieService: CookieService) { 

        this.utilisateur = this._sessionSt.retrieve("user");
        this.commande = this.cookieService.getObject("commande");
        console.log((this.commande._oeuvre)[0]);
        this.prixData = this.commande._prix;

        (document.getElementsByTagName('body')[0]).addEventListener("click", ()=>{
            if (this.accueil){
                this.accueil = false;
                this._router.navigateByUrl("welcome");
                window.location.reload();
            }
        });

    }

    ngOnInit() { this.init(); }

    init() {
        var current = this;
        this.stripe = Stripe('pk_test_E1MO1eEBxsp7vKiWHs9vEWGk');
        var elements = this.stripe.elements();

        var card = elements.create('card', {
            style: {
                base: {
                    iconColor: '#666EE8',
                    color: '#31325F',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '15px',

                    '::placeholder': {
                        color: '#CFD7E0',
                    },
                },
            }
        });
        card.mount('#card-element');
        card.on('change', function (event) {
            current.setOutcome(event);
        });

        document.querySelector('form').addEventListener('submit', function (e) {
            console.log("in submit")
            e.preventDefault();
            var form = document.querySelector('form')[0];
            console.log("in form", form)
            var extraDetails = {
                name: form.querySelector('input[name=cardholder-name]').value,
            };
            current.stripe.createToken(card, extraDetails).then(current.setOutcome);
        });
    }

    setOutcome(result) {
        var successElement = document.querySelector('.success');
        var errorElement = document.querySelector('.error');
        successElement.classList.remove('visible');
        errorElement.classList.remove('visible');

        if (result.token) {
            // Use the token to create a charge or a customer
            // https://stripe.com/docs/charges
            successElement.querySelector('.token').textContent = result.token.id;
            successElement.classList.add('visible');
        } else if (result.error) {
            errorElement.textContent = result.error.message;
            errorElement.classList.add('visible');
        }
    }

    ngAfterViewInit() {
        window.scroll(0, 0);
    }

    onValidation(){
        if (this.utilisateur.prenom!="" && this.utilisateur.nom!="" && this.utilisateur.telephone!="" && this.utilisateur.adresse.rue!="" && this.utilisateur.adresse.codePostal!="" && this.utilisateur.adresse.ville!="" && this.utilisateur.adresse.pays!=""){
            let json = "{" +
              "\"civilite\":\"" + this.utilisateur.civilite + "\"," +
              "\"nom\":\"" + this.utilisateur.nom + "\"," +
              "\"prenom\":\"" + this.utilisateur.prenom + "\"," +
              "\"telephone\":\"" + this.utilisateur.telephone + "\"," +
              "\"rue\":\"" + this.utilisateur.adresse.rue + "\"," +
              "\"codePostal\":\"" + this.utilisateur.adresse.codePostal + "\"," +
              "\"ville\":\"" + this.utilisateur.adresse.ville + "\"," +
              "\"pays\":\"" + this.utilisateur.adresse.pays + "\"}";

            let json2 = "{" +
              "\"id\":" + this.utilisateur.id + "," +
              "\"prix\":" + this.commande._prix + "}";

            let json3 = "[";
            for (let i=0; i<this.commande._oeuvre.length-1; i++){
                json3 += "{" +
                    "\"id\":" + (this.commande._oeuvre)[i]._id + "," +
                    "\"quantite\":" + (this.commande._oeuvre)[i]._quantite + "," +
                    "\"prixAchat\":" + (this.commande._oeuvre)[i]._prixAchat + "," +
                    "\"finition\":" + Finition[(this.commande._oeuvre)[i]._finition] + "," +
                    "\"formatChoisi\":" + Format[(this.commande._oeuvre)[i]._formatChoisi] + "," +
                    "\"cadre\":" + Cadre[(this.commande._oeuvre)[i]._cadre] + "},";
            }
            json3 += "{" +
                    "\"id\":" + (this.commande._oeuvre)[(this.commande._oeuvre.length)-1]._id + "," +
                    "\"quantite\":" + (this.commande._oeuvre)[(this.commande._oeuvre.length)-1]._quantite + "," +
                    "\"prixAchat\":" + (this.commande._oeuvre)[(this.commande._oeuvre.length)-1]._prixAchat + "," +
                    "\"finition\":" + Finition[(this.commande._oeuvre)[(this.commande._oeuvre.length)-1]._finition] + "," +
                    "\"formatChoisi\":" + Format[(this.commande._oeuvre)[(this.commande._oeuvre.length)-1]._formatChoisi] + "," +
                    "\"cadre\":" + Cadre[(this.commande._oeuvre)[(this.commande._oeuvre.length)-1]._cadre] + "}]";
            console.log(json3);
            Promise.resolve().then(() => {
                this.formValide = true
            }).then(() => {
                this.utilisateursService.postModifierUtilisateur(this._sessionSt.retrieve("user").id , json).subscribe(()=>{
                    this.commandesService.postNouvelleCommande(json2).subscribe(() => {
                        this.commandesService.postNouvelleCommandesOeuvres(json3).subscribe(()=>{
                            this.cookieService.putObject("oeuvres",new Array<Oeuvre>());
                            this.cookieService.remove("commande");
                            this.accueil = true;
                        });
                    });
                });
            });
        } else {
            this.formValide = false;
            this.accueil = false;
        }
    }

}
