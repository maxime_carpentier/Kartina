import { Component, OnInit, Output, EventEmitter, OnChanges, Input } from '@angular/core';
import { Finition } from '../enum/Finition';

@Component({
  selector: 'app-finition',
  templateUrl: './finition.component.html',
  styleUrls: ['./finition.component.scss']
})
export class FinitionComponent implements OnInit, OnChanges {

  private classique : boolean = false;
  private autres_formats : boolean = true;
  
  @Output() finitionData = new EventEmitter<Finition>();
  @Output() etapeCadre = new EventEmitter<boolean>();
  @Output() etapeFormat = new EventEmitter<boolean>();
  @Output() etapeAchat = new EventEmitter<boolean>();
  private choixFinition: boolean = false;
  
  
  private choixTirage: boolean = false;
  
  
  
  @Input() formatChoisi: any;

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges()
  {
  }
  
  choixSupportAluminium()
  {
  	this.choixFinition = true;
  	this.choixTirage = false;
    this.finitionData.emit(Finition.ALUMINIUM);
    this.surlignementChoix("aluminium");
  	console.log("support aluminium");
  }
  
  choixSupportAluminiumAvecVerreAcrylique()
  {
  	this.choixFinition = true;
  	this.choixTirage = false;
    this.finitionData.emit(Finition.VERRE_ACRYLIQUE);
    this.surlignementChoix("acrylique");
  	console.log("support aluminium avec verre acrylique");
  }
  
  choixTirageSurPapierPhoto()
  {
  	this.choixFinition = false;
  	this.choixTirage = true;
    this.finitionData.emit(Finition.PAPIER_PHOTO);
    this.surlignementChoix("papierphoto");
  	console.log("tirage sur papier photo");
  }
  
  choixBlackout()
  {
  	this.choixFinition = true;
    this.finitionData.emit(Finition.BLACKOUT);
    this.surlignementChoix("ppnoir");
  	console.log("blackout");
  }
  
  choixArtshot()
  {
  	this.choixFinition = true;
    this.finitionData.emit(Finition.ARTSHOT);
    this.surlignementChoix("ppblanc");
  	console.log("artshot");
  }

  surlignementChoix(choix: string) {
    let choixPossibles: Array<string>;
    if (this.formatChoisi==0){
      choixPossibles = ["ppnoir", "ppblanc"];
    } else {
      choixPossibles = ["aluminium", "acrylique", "papierphoto"];
    }
    for (let i = 0; i < choixPossibles.length; i++) {
      (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseenter = function () {
      };
      (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseleave = function () {
      };
      if (choixPossibles[i] == choix) {
        console.log("if", choixPossibles[i]);
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#544a3f";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "white";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "none";
      } else {
        console.log("else", choixPossibles[i]);
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#f9fbff";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "#544a3f";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "2px solid #a89683";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseenter = function () {
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#544a3f";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "white";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "none";
        };
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseleave = function () {
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#f9fbff";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "#544a3f";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "2px solid #a89683";
        };
      }
    }
  }
  
  versCadre()
  {
  	this.etapeCadre.emit(true);
  }
  
  versFormat()
  {
  	this.etapeFormat.emit(true);
  }
  
  versAchat()
  {
    console.log("vers achat");
    this.etapeAchat.emit(true);
  }


  

}
