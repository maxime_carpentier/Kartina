import { Component, OnInit } from '@angular/core';
import { OeuvresService } from '../services/oeuvres.service';
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
    selector: 'app-photographies',
    templateUrl: './photographies.component.html',
    styleUrls: ['./photographies.component.scss']
})
export class PhotographiesComponent implements OnInit {

    ariane1: string = "Photographies";
    ariane2: string = "Nature";
    linkAriane1: string = "/photographies";
    boolAriane2: boolean = false;

    private oeuvres: any;
    private nbOeuvresNbPages: any;
    private nbOeuvres: any;
    private arrayNbPages: Array<number> = new Array<number>();

    pageactuelle: number = 1;
    precedente: boolean = false;
    suivante: boolean = true;
    arrayPages = document.getElementsByClassName("page_numero") as HTMLCollectionOf<HTMLElement>;
    arrayTris = document.getElementsByClassName("type_tri") as HTMLCollectionOf<HTMLElement>;
    arrayThemes = document.getElementsByClassName("theme_choisi") as HTMLCollectionOf<HTMLElement>;

    orderBy: string = "date_mise_en_vente";
    ascdesc: string = "desc";
    derniers: string = "false";
    nouveautes: string = "false";
    nomTheme: string = "false";

    orientation: string = "false";
    format: string = "false";
    prix: string = "false";

    constructor(private _sessionSt: SessionStorageService, private _cookieService: CookieService, private _oeuvresService: OeuvresService, public http: HttpClient, private _router: Router, private activatedRoute: ActivatedRoute) {

        this._cookieService.put("bjr", "bonjour");
        this._cookieService.put("bjr", "bjjr");

        Promise.resolve().then(() => {
            this.onPage(1);
        })
            .then(() => {
                this._oeuvresService.getNbreOeuvres(this.derniers, this.nouveautes, this.nomTheme, this.orientation, this.format, this.prix).subscribe(data => {
                    this.nbOeuvresNbPages = data;
                    for (let i = 1; i <= this.nbOeuvresNbPages.nbPages; i++) {
                        this.arrayNbPages.push(i);
                    }
                });
            });

    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(routeParams => {
            if (routeParams.neworlast == "nouveautes") {
                console.log(this.nouveautes);
                for (let i = 0; i < this.arrayTris.length; i++) {
                    this.arrayTris[i].style.color = "#544a3f";
                    this.arrayTris[i].style.cursor = "pointer";
                }
                for (let i = 0; i < this.arrayThemes.length; i++) {
                    this.arrayThemes[i].style.color = "#544a3f";
                    this.arrayThemes[i].style.cursor = "pointer";
                }
                this.orientation = "false";
                this.effacerSelectionOrientation();
                this.format = "false";
                this.effacerSelectionFormat();
                this.prix = "false";
                this.effacerSelectionPrix();
                this.nomTheme = "false";
                this.nouveautes = "true";
                this.derniers = "false";
                this.boolAriane2 = false;
                this.orderBy = "date_mise_en_vente";
                this.ascdesc = "desc";
                console.log(this.nouveautes);
                this.onPage(1);
            }
            if (routeParams.neworlast == "derniers") {
                for (let i = 0; i < this.arrayTris.length; i++) {
                    this.arrayTris[i].style.color = "#544a3f";
                    this.arrayTris[i].style.cursor = "pointer";
                }
                for (let i = 0; i < this.arrayThemes.length; i++) {
                    this.arrayThemes[i].style.color = "#544a3f";
                    this.arrayThemes[i].style.cursor = "pointer";
                }
                this.orientation = "false";
                this.effacerSelectionOrientation();
                this.format = "false";
                this.effacerSelectionFormat();
                this.prix = "false";
                this.effacerSelectionPrix();
                this.nomTheme = "false";
                this.nouveautes = "false";
                this.derniers = "true";
                this.orderBy = "date_mise_en_vente";
                this.ascdesc = "desc";
                this.boolAriane2 = false;
                this.onPage(1);
            }
            if (routeParams.neworlast == "noir_et_blanc") {
                for (let i = 0; i < this.arrayTris.length; i++) {
                    this.arrayTris[i].style.color = "#544a3f";
                    this.arrayTris[i].style.cursor = "pointer";
                }
                for (let i = 0; i < this.arrayThemes.length; i++) {
                    this.arrayThemes[i].style.color = "#544a3f";
                    this.arrayThemes[i].style.cursor = "pointer";
                }
                this.arrayThemes[0].style.color = "#a89683";
                this.arrayThemes[0].style.cursor = "auto";
                this.orientation = "false";
                this.effacerSelectionOrientation();
                this.format = "false";
                this.effacerSelectionFormat();
                this.prix = "false";
                this.effacerSelectionPrix();
                this.nomTheme = "1";
                this.nouveautes = "false";
                this.derniers = "false";
                this.orderBy = "date_mise_en_vente";
                this.ascdesc = "desc";
                this.boolAriane2 = true;
                this.ariane2 = "Noir & Blanc";
                this.onPage(1);
            }
            if (routeParams.neworlast == "voyages") {
                for (let i = 0; i < this.arrayTris.length; i++) {
                    this.arrayTris[i].style.color = "#544a3f";
                    this.arrayTris[i].style.cursor = "pointer";
                }
                for (let i = 0; i < this.arrayThemes.length; i++) {
                    this.arrayThemes[i].style.color = "#544a3f";
                    this.arrayThemes[i].style.cursor = "pointer";
                }
                this.arrayThemes[1].style.color = "#a89683";
                this.arrayThemes[1].style.cursor = "auto";
                this.orientation = "false";
                this.effacerSelectionOrientation();
                this.format = "false";
                this.effacerSelectionFormat();
                this.prix = "false";
                this.effacerSelectionPrix();
                this.nomTheme = "2";
                this.nouveautes = "false";
                this.derniers = "false";
                this.orderBy = "date_mise_en_vente";
                this.ascdesc = "desc";
                this.boolAriane2 = true;
                this.ariane2 = "Voyages";
                this.onPage(1);
            }
            if (routeParams.neworlast == "meilleures_ventes") {
                for (let i = 0; i < this.arrayTris.length; i++) {
                    this.arrayTris[i].style.color = "#544a3f";
                    this.arrayTris[i].style.cursor = "pointer";
                }
                for (let i = 0; i < this.arrayThemes.length; i++) {
                    this.arrayThemes[i].style.color = "#544a3f";
                    this.arrayThemes[i].style.cursor = "pointer";
                }
                this.arrayTris[1].style.color = "#a89683";
                this.arrayTris[1].style.cursor = "auto";
                this.orientation = "false";
                this.effacerSelectionOrientation();
                this.format = "false";
                this.effacerSelectionFormat();
                this.prix = "false";
                this.effacerSelectionPrix();
                this.nomTheme = "false";
                this.nouveautes = "false";
                this.derniers = "false";
                this.boolAriane2 = false;
                this.orderBy = "(nb_tirages-nb_tirages_restants)";
                this.ascdesc = "desc";
                this.onPage(1);
            }
        });

    }

    ngAfterViewInit() {
        window.scroll(0, 0);
    }

    ngAfterViewChecked() {
        for (let i = 0; i < this.arrayPages.length; i++) {
            this.arrayPages[i].style.color = "#544a3f";
            this.arrayPages[i].style.cursor = "pointer";
        }
        this.arrayPages[this.pageactuelle - 1].style.color = "#a89683";
        this.arrayPages[this.pageactuelle - 1].style.cursor = "auto";

    }

    onPage(page: number) {

        this.pageactuelle = page;

        Promise.resolve().then(() => {
            this._oeuvresService.getOeuvres(page, this.orderBy, this.ascdesc, this.derniers, this.nomTheme, this.nouveautes, this.orientation, this.format, this.prix).subscribe(data => {
                this.oeuvres = data;
            });
        })
            .then(() => {
                this._oeuvresService.getNbreOeuvres(this.derniers, this.nouveautes, this.nomTheme, this.orientation, this.format, this.prix).subscribe(data => {
                    this.nbOeuvresNbPages = data;
                    this.nbOeuvres = this.nbOeuvresNbPages.nb_oeuvres;
                    this.arrayNbPages = new Array<number>();
                    for (let i = 1; i <= this.nbOeuvresNbPages.nbPages; i++) {
                        this.arrayNbPages.push(i);
                    }
                });
            })
            .then(() => {
                console.log(page);
                console.log(this.arrayNbPages.length);
                if (page != this.arrayNbPages.length) {
                    this.suivante = true;
                } else {
                    this.suivante = false;
                }
                if (page != 1) {
                    this.precedente = true;
                } else {
                    this.precedente = false;
                }

                window.scroll(0, 0);
            });


    }

    onPrecedente() {
        this.pageactuelle -= 1;
        this.onPage(this.pageactuelle);
    }

    onSuivante() {
        this.pageactuelle += 1;
        this.onPage(this.pageactuelle);
    }

    effacerSelectionOrientation() {
        this.orientation = "false";
        (document.getElementById("portrait") as HTMLInputElement).checked = false;
        (document.getElementById("paysage") as HTMLInputElement).checked = false;
        (document.getElementById("carre") as HTMLInputElement).checked = false;
        (document.getElementById("panoramique") as HTMLInputElement).checked = false;
        this.onPage(1);
    }

    effacerSelectionFormat() {
        this.format = "false";
        (document.getElementById("classique") as HTMLInputElement).checked = false;
        (document.getElementById("grand") as HTMLInputElement).checked = false;
        (document.getElementById("geant") as HTMLInputElement).checked = false;
        (document.getElementById("collector") as HTMLInputElement).checked = false;
        this.onPage(1);
    }

    effacerSelectionPrix() {
        this.prix = "false";
        (document.getElementById("prix1") as HTMLInputElement).checked = false;
        (document.getElementById("prix2") as HTMLInputElement).checked = false;
        (document.getElementById("prix3") as HTMLInputElement).checked = false;
        (document.getElementById("prix4") as HTMLInputElement).checked = false;
        (document.getElementById("prix5") as HTMLInputElement).checked = false;
        (document.getElementById("prix6") as HTMLInputElement).checked = false;
        this.onPage(1);
    }

    onOrientation() {
        this.nouveautes = "false";
        this.orientation = "(";
        let portrait = document.getElementById("portrait") as HTMLInputElement;
        let paysage = document.getElementById("paysage") as HTMLInputElement;
        let carre = document.getElementById("carre") as HTMLInputElement;
        let panoramique = document.getElementById("panoramique") as HTMLInputElement;
        this.orientation += (portrait.checked ? "id_orientation=4" : "") +
            (portrait.checked && paysage.checked ? " or id_orientation=2" : (paysage.checked ? "id_orientation=2" : "")) +
            ((portrait.checked || paysage.checked) && carre.checked ? " or id_orientation=1" : (carre.checked ? "id_orientation = 1" : "")) +
            ((portrait.checked || paysage.checked || carre.checked) && panoramique.checked ? " or id_orientation=3" : (panoramique.checked ? "id_orientation = 3" : ""));
        this.orientation += ")";
        if (this.orientation == "()") {
            this.orientation = "false";
        }
        this.onPage(1);
    }

    onFormat() {
        this.nouveautes = "false";
        this.format = "(";
        let classique = document.getElementById("classique") as HTMLInputElement;
        let grand = document.getElementById("grand") as HTMLInputElement;
        let geant = document.getElementById("geant") as HTMLInputElement;
        let collector = document.getElementById("collector") as HTMLInputElement;
        this.format += (collector.checked ? "id_format=4" : "") +
            (collector.checked && grand.checked ? " or id_format=2" : (grand.checked ? "id_format=2" : "")) +
            ((collector.checked || grand.checked) && classique.checked ? " or id_format=1" : (classique.checked ? "id_format = 1" : "")) +
            ((collector.checked || grand.checked || classique.checked) && geant.checked ? " or id_format=3" : (geant.checked ? "id_format = 3" : ""));
        this.format += ")";
        if (this.format == "()") {
            this.format = "false";
        }
        this.onPage(1);
    }

    onPrix() {
        this.nouveautes = "false";
        this.prix = "(";
        let prix1 = document.getElementById("prix1") as HTMLInputElement;
        let prix2 = document.getElementById("prix2") as HTMLInputElement;
        let prix3 = document.getElementById("prix3") as HTMLInputElement;
        let prix4 = document.getElementById("prix4") as HTMLInputElement;
        let prix5 = document.getElementById("prix5") as HTMLInputElement;
        let prix6 = document.getElementById("prix6") as HTMLInputElement;
        this.prix += (prix1.checked ? "prix_de_base<=50" : "") +
            (prix1.checked && prix2.checked ? " or (prix_de_base>50 and prix_de_base<=100)" : (prix2.checked ? "(prix_de_base>50 and prix_de_base<=100)" : "")) +
            ((prix1.checked || prix2.checked) && prix3.checked ? " or (prix_de_base>100 and prix_de_base<=200)" : (prix3.checked ? "(prix_de_base>100 and prix_de_base<=200)" : "")) +
            ((prix1.checked || prix2.checked || prix3.checked) && prix4.checked ? " or (prix_de_base>200 and prix_de_base<=500)" : (prix4.checked ? "(prix_de_base>200 and prix_de_base<=500)" : "")) +
            ((prix1.checked || prix2.checked || prix3.checked || prix4.checked) && prix5.checked ? " or (prix_de_base>500 and prix_de_base<=1000)" : (prix5.checked ? "(prix_de_base>500 and prix_de_base<=1000)" : "")) +
            ((prix1.checked || prix2.checked || prix3.checked || prix4.checked || prix5.checked) && prix6.checked ? " or (prix_de_base>1000)" : (prix6.checked ? "(prix_de_base>1000)" : ""));
        this.prix += ")";
        if (this.prix == "()") {
            this.prix = "false";
        }
        this.onPage(1);
    }

    modifOrderBy(event) {
        for (let i = 0; i < this.arrayTris.length; i++) {
            this.arrayTris[i].style.color = "#544a3f";
            this.arrayTris[i].style.cursor = "pointer";
        }
        let tri: string = event.target.textContent;
        if (tri == "Nouveautés") {
            this.orderBy = "date_mise_en_vente";
            this.ascdesc = "desc";
        } else if (tri == "Meilleures ventes") {
            this.orderBy = "(nb_tirages-nb_tirages_restants)";
            this.ascdesc = "desc";
        } else if (tri == "Prix croissants") {
            this.orderBy = "prix_de_base";
            this.ascdesc = "asc";
        } else if (tri == "Prix décroissants") {
            this.orderBy = "prix_de_base";
            this.ascdesc = "desc";
        }
        event.target.style.color = "#a89683";
        event.target.style.cursor = "auto";
        this.onPage(1);

    }

    onTheme(event, id: string) {
        this.format = "false";
        this.effacerSelectionFormat();
        this.orientation = "false";
        this.effacerSelectionOrientation();
        for (let i = 0; i < this.arrayTris.length; i++) {
            this.arrayTris[i].style.color = "#544a3f";
            this.arrayTris[i].style.cursor = "pointer";
        }
        for (let i = 0; i < this.arrayThemes.length; i++) {
            this.arrayThemes[i].style.color = "#544a3f";
            this.arrayThemes[i].style.cursor = "pointer";
        }
        event.target.style.color = "#a89683";
        event.target.style.cursor = "auto";
        this.boolAriane2 = true;
        this.nomTheme = id;
        switch (id) {
            case '1': {
                this.ariane2 = "Noir & Blanc";
                break;
            }
            case '2': {
                this.ariane2 = "Voyages";
                break;
            }
            case '3': {
                this.ariane2 = "Nourriture";
                break;
            }
            case '4': {
                this.ariane2 = "Sports";
                break;
            }
            case '5': {
                this.ariane2 = "Musique";
                break;
            }
            case '6': {
                this.ariane2 = "Animaux";
                break;
            }
            default: {
                break;
            }
        }
        this.nouveautes = "false";
        this.derniers = "false";
        this.orderBy = "date_mise_en_vente";
        this.ascdesc = "desc";
        this.onPage(1);
    }



    routingToAchat(id: number) {
        this._router.navigate(['/achat', id]);
    }

    routingToArtiste(id: number) {
        this._router.navigate(['/artiste', id]);
    }


}
