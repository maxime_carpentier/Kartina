import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographiesComponent } from './photographies.component';

describe('PhotographiesComponent', () => {
  let component: PhotographiesComponent;
  let fixture: ComponentFixture<PhotographiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
