export class Adresse {

  constructor() {

  }

  private _rue: string;

  public get rue(): string {
    return this._rue;
  }

  public set rue(value: string) {
    this._rue = value;
  }

  private _codePostal: string;

  public get codePostal(): string {
    return this._codePostal;
  }

  public set codePostal(value: string) {
    this._codePostal = value;
  }

  private _ville: string;

  public get ville(): string {
    return this._ville;
  }

  public set ville(value: string) {
    this._ville = value;
  }

  private _pays: string;

  public get pays(): string {
    return this._pays;
  }

  public set pays(value: string) {
    this._pays = value;
  }

}
