import {Adresse} from './Adresse';
import {Commande} from './Commande';

export class Client {

  constructor() {

  }

  private _id: number;

  public get id(): number {
    return this._id;
  }

  public set id(value: number) {
    this._id = value;
  }

  private _civilite: string;

  public get civilite(): string {
    return this._civilite;
  }

  public set civilite(value: string) {
    this._civilite = value;
  }

  private _prenom: string;

  public get prenom(): string {
    return this._prenom;
  }

  public set prenom(value: string) {
    this._prenom = value;
  }

  private _nom: string;

  public get nom(): string {
    return this._nom;
  }

  public set nom(value: string) {
    this._nom = value;
  }

  private _email: string;

  public get email(): string {
    return this._email;
  }

  public set email(value: string) {
    this._email = value;
  }

  private _motDePasse: string;

  public get motDePasse(): string {
    return this._motDePasse;
  }

  public set motDePasse(value: string) {
    this._motDePasse = value;
  }

  private _telephone: string;

  public get telephone(): string {
    return this._telephone;
  }

  public set telephone(value: string) {
    this._telephone = value;
  }

  private _adresse: Adresse;

  public get adresse(): Adresse {
    return this._adresse;
  }

  public set adresse(value: Adresse) {
    this._adresse = value;
  }

  private _commandes: Array<Commande> = new Array<Commande>();

  public get commandes(): Array<Commande> {
    return this._commandes;
  }

  public set commandes(value: Array<Commande>) {
    this._commandes = value;
  }

  private _compteBloque = false;

  public get compteBloque(): boolean {
    return this._compteBloque;
  }

  public set compteBloque(value: boolean) {
    this._compteBloque = value;
  }

  private _admin = false;

  public get admin(): boolean {
    return this._admin;
  }

  public set admin(value: boolean) {
    this._admin = value;
  }

  private _artiste = false;

  public get artiste(): boolean {
    return this._artiste;
  }

  public set artiste(value: boolean) {
    this._artiste = value;
  }

}
