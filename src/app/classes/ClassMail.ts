
export class ClassMail {

    private _sujet: string;
    private _message: string;
    
    constructor() {
    }
    public get sujet(): string {
        return this._sujet;
    }
    public set sujet(value: string) {
        this._sujet = value;
    }
    public get message(): string {
        return this._message;
    }
    public set message(value: string) {
        this._message = value;
    }
}