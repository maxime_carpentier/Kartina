import { Theme } from "../enum/Theme";
import { Orientation } from "../enum/Orientation";
import { Format } from "../enum/Format";
import { Artiste } from "./Artiste";
import { Finition } from "../enum/Finition";
import { Cadre } from "../enum/Cadre";

export class Oeuvre {

    private _id: number;
    private _themes: Array<Theme> = new Array<Theme>();
    private _orientation: Orientation;
    private _formats: Array<Format> = new Array<Format>();
    private _prixDeBase: number;
    private _prixAchat : number;
    private _artiste: Artiste;
    private _titrePhoto: string;
    private _lienImage: string;
    private _nbTirages: number;
    private _nbTiragesRestants: number;
    private _dateMiseEnOeuvre: Date;
    private _finition: Finition;
    private _cadre: Cadre;
    private _quantite: number;
    private _formatChoisi: Format;
    private _dateFinVente: Date;

    constructor(){

    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get themes(): Array<Theme> {
        return this._themes;
    }
    public set themes(value: Array<Theme>) {
        this._themes = value;
    }
    public get orientation(): Orientation {
        return this._orientation;
    }
    public set orientation(value: Orientation) {
        this._orientation = value;
    }
    public get formats(): Array<Format> {
        return this._formats;
    }
    public set formats(value: Array<Format>) {
        this._formats = value;
    }
    public get prixDeBase(): number {
        return this._prixDeBase;
    }
    public set prixDeBase(value: number) {
        this._prixDeBase = value;
    }
    public get prixAchat(): number {
        return this._prixAchat;
    }
    public set prixAchat(value: number) {
        this._prixAchat = value;
    }
    public get artiste(): Artiste {
        return this._artiste;
    }
    public set artiste(value: Artiste) {
        this._artiste = value;
    }
    public get titrePhoto(): string {
        return this._titrePhoto;
    }
    public set titrePhoto(value: string) {
        this._titrePhoto = value;
    }
    public get lienImage(): string {
        return this._lienImage;
    }
    public set lienImage(value: string) {
        this._lienImage = value;
    }
    public get nbTirages(): number {
        return this._nbTirages;
    }
    public set nbTirages(value: number) {
        this._nbTirages = value;
    }
    public get nbTiragesRestants(): number {
        return this._nbTiragesRestants;
    }
    public set nbTiragesRestants(value: number) {
        this._nbTiragesRestants = value;
    }
    public get dateMiseEnOeuvre(): Date {
        return this._dateMiseEnOeuvre;
    }
    public set dateMiseEnOeuvre(value: Date) {
        this._dateMiseEnOeuvre = value;
    }
    public get dateFinVente(): Date {
        return this._dateFinVente;
    }
    public set dateFinVente(value: Date) {
        this._dateFinVente = value;
    }
    public get finition(): Finition {
        return this._finition;
    }
    public set finition(value: Finition) {
        this._finition = value;
    }
    public get cadre(): Cadre {
        return this._cadre;
    }
    public set cadre(value: Cadre) {
        this._cadre = value;
    }
    public get quantite(): number {
        return this._quantite;
    }
    public set quantite(value: number) {
        this._quantite = value;
    }
    public get formatChoisi(): Format {
        return this._formatChoisi;
    }
    public set formatChoisi(formatChoisi: Format) {
        this._formatChoisi = formatChoisi;
    }

}