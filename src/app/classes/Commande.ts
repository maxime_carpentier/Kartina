import { Client } from "./Client";
import { Oeuvre } from "./Oeuvre";
import { Statut } from "../enum/Statut";

export class Commande {

    private _id: number;
    private _oeuvre: Array<Oeuvre> = new Array<Oeuvre>();
    private _client: Client;
    private _dateAchat = Date;
    private _statut: Statut;
    private _prix : number;

    
    constructor(){

    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get prix(): number {
        return this._prix;
    }
    public set prix(value: number) {
        this._prix = value;
    }
    public get oeuvre(): Array<Oeuvre> {
        return this._oeuvre;
    }
    public set oeuvre(value: Array<Oeuvre>) {
        this._oeuvre = value;
    }    
    public get client(): Client {
        return this._client;
    }
    public set client(value: Client) {
        this._client = value;
    }    
    public get dateAchat() {
        return this._dateAchat;
    }
    public set dateAchat(value) {
        this._dateAchat = value;
    }    
    public get statut(): Statut {
        return this._statut;
    }
    public set statut(value: Statut) {
        this._statut = value;
    }

}