import { Oeuvre } from "./Oeuvre";
import { Client } from "./Client";

export class Artiste extends Client {

    private _oeuvres: Array<Oeuvre> = new Array<Oeuvre>();
    private _biographie: string;

    constructor(){
        super();
    }

    public get oeuvres(): Array<Oeuvre> {
        return this._oeuvres;
    }
    public set oeuvres(value: Array<Oeuvre>) {
        this._oeuvres = value;
    }
    public get biographie(): string {
        return this._biographie;
    }
    public set biographie(value: string) {
        this._biographie = value;
    }




}