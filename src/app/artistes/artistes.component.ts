import { Component, OnInit } from '@angular/core';
import { ArtistesService } from '../services/artistes.service';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-artistes',
  templateUrl: './artistes.component.html',
  styleUrls: ['./artistes.component.scss']
})
export class ArtistesComponent implements OnInit {

  ariane1: string = "Artistes";

  alphabet: Array<string> = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  arrayPages = document.getElementsByClassName("page_numero") as HTMLCollectionOf<HTMLElement>;

  artistes: any;
  lettreActuelle: string;

  constructor(private _sessionSt: SessionStorageService, private _cookieService: CookieService, private _artistesService: ArtistesService, public http: HttpClient) {

    console.log(this._cookieService.get("bjr"));
    this.onPage('A');
    console.log(this._sessionSt.retrieve("nbPages"));

  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    for (let i = 0; i < this.arrayPages.length; i++) {
      this.arrayPages[i].style.color = "#544a3f";
      this.arrayPages[i].style.cursor = "pointer";
    }
    this.arrayPages[this.lettreActuelle.charCodeAt(0) - 65].style.color = "#a89683";
    this.arrayPages[this.lettreActuelle.charCodeAt(0) - 65].style.cursor = "auto";

  }



  onPage(lettre: string) {
    this.lettreActuelle = lettre;
    this._artistesService.getArtistes(lettre).subscribe(data => {
      this.artistes = data;
    });
  }


}
