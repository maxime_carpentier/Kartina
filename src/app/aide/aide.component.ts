import { Component, OnInit } from '@angular/core';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Aide } from '../classes/Aide';
import { Client } from '../classes/Client';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aide',
  templateUrl: './aide.component.html',
  styleUrls: ['./aide.component.scss']
})
export class AideComponent implements OnInit {

  ariane1 = "Aide";
  formValide = false;
  dataclient: Aide = new Aide();
  accueil: boolean;

  // Définition du constructeur qui fait appel au service avec un GET pour Recevoir les Variables      constructor() { }
  constructor(private _router: Router, private sessionSt: SessionStorageService, private _utilisateursService: UtilisateursService) {
    (document.getElementsByTagName('body')[0]).addEventListener("click", () => {
      if (this.accueil) {
        this.accueil = false;
        this._router.navigateByUrl("welcome");
        window.location.reload();
      }
    });
    this.dataclient.user = new Client();
    if (this.sessionSt.retrieve("user")) {
      this.dataclient.user = this.sessionSt.retrieve("user");
    }

    /* this._utilisateursService.getUtilisateurClient().subscribe(data => {
       this.dataclient.user = data;
     });*/
  }

  ngOnInit() {
  }

  // Méthode qui fait appel au service pour appeler ensuite la servlet qui va Envoyer un mail au client
  EnvoiMail() {
    if (this.dataclient.user.nom != "" && this.dataclient.user.prenom != "" && this.dataclient.user.civilite != "" && this.dataclient.user.telephone != "" && this.dataclient.user.email != "" && this.dataclient.sujet != null && this.dataclient.message != null) {
      this.formValide = true;
      this._utilisateursService.postEnvoiEmailClientAide(this.dataclient).subscribe(() => {
        this.accueil = true;
      });
    } else {
      this.formValide = false;
    }
  }
}
