import { Component, OnInit } from '@angular/core';
import { UtilisateursService } from '../services/utilisateurs.service';
import { HttpClient } from '@angular/common/http';
import { Adresse } from '../classes/Adresse';
import { Router } from '@angular/router';
import { CommandesService } from '../services/commandes.service';
import { Oeuvre } from '../classes/Oeuvre';
import { OeuvresService } from '../services/oeuvres.service';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  private donnees: boolean = true;
  private adresse: boolean = false;
  private commandes: boolean = false;
  private vendeur: boolean = false;
  private admin: boolean = false;
  private nouvelle_vente: boolean = false;
  private vente_en_cours: boolean = false;
  private ventes_passees: boolean = false;
  private biographie: boolean = false;
  private activ_vendeurs: boolean = false;
  private statut_commandes: boolean = false;

  ariane1: string = "Mon compte";
  sessionclient: any;
  utilisateurs: any;
  commandesList: any;
  commandesParUtilisateur: any;
  commandes_id: any;
  oeuvresList_id: Array<Oeuvre> = new Array<Oeuvre>();
  oeuvresCourantes: any;
  oeuvresPassees: any;

  address: Adresse;

  json: string = "";

  selectedFile: File;
  titre: string;
  nb_tirages: number;
  tarif: number;
  biographieData : string;

  arrayCivilites = document.getElementsByClassName("civilite") as HTMLCollectionOf<HTMLElement>;
  arrayNoms = document.getElementsByClassName("nomPrenom") as HTMLCollectionOf<HTMLElement>;
  arrayVendeurs = document.getElementsByClassName("artisteCheck") as HTMLCollectionOf<HTMLElement>;
  arrayBloques = document.getElementsByClassName("bloqueCheck") as HTMLCollectionOf<HTMLElement>;
  arrayCommandes = document.getElementsByClassName("commande") as HTMLCollectionOf<HTMLElement>;
  arrayCommandesId = document.getElementsByClassName("id_commande") as HTMLCollectionOf<HTMLElement>;
  arrayCommandesStatuts = document.getElementsByClassName("statuts") as HTMLCollectionOf<HTMLElement>;
  arrayFormatsChoisis = document.getElementsByClassName("formats_choisis") as HTMLCollectionOf<HTMLElement>;
  arrayThemesChoisis = document.getElementsByClassName("themes_choisis") as HTMLCollectionOf<HTMLElement>;

  constructor(private _sessionSt: SessionStorageService, private _oeuvresService: OeuvresService, private _commandesService: CommandesService, private _utilisateursService: UtilisateursService, public http: HttpClient, public router: Router) {

    this.onUtilisateurSession();
    this.onListeUtilisateurs();
    this.onListeCommandes();
    this.onListeCommandesCourantes();
    this.onListeCommandesPassees();
    this.onListeCommandesParUtilisateur();

  }

  ngOnInit() {
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onSubmitInfosUser(){
    this.json = "{\"civilite\":";
    switch ((this.arrayCivilites[0] as HTMLSelectElement).options[(this.arrayCivilites[0] as HTMLSelectElement).selectedIndex].text) {
      case "M": { this.json += "\"M\","; break; }
      case "Mme": { this.json += "\"Mme\","; break; }
      case "Melle": { this.json += "\"Melle\","; break; }
      default: { this.json += "..."; break; }
    }
    this.json += "\"nom\":\"" + this.sessionclient.nom + "\"," +
      "\"prenom\":\"" + this.sessionclient.prenom + "\"," +
      "\"telephone\":\"" + this.sessionclient.telephone + "\"" +
      "}";
    console.log(this.json);
    this._utilisateursService.postUpdateInfosUtilisateur(this._sessionSt.retrieve("user").id, this.json).subscribe(() => {
      this.onUtilisateurSession();
    });
  }

  onSubmitAdresse() {
    this.json = "{";
    this.json += "\"rue\":\"" + this.sessionclient.adresse.rue + "\"," +
      "\"codePostal\":\"" + this.sessionclient.adresse.codePostal + "\"," +
      "\"ville\":\"" + this.sessionclient.adresse.ville + "\"," +
      "\"pays\":\"" + this.sessionclient.adresse.pays + "\"" +
      "}";
    console.log(this.json);
    this._utilisateursService.postUpdateAdresseUtilisateur(this._sessionSt.retrieve("user").id, this.json).subscribe(() => {
      this.onUtilisateurSession();
    });    
  }

  onSubmitActivVendeurs() {
    this.json = "[";
    for (let i = 0; i < this.arrayBloques.length - 1; i++) {
      this.json += "{" +
        "\"id\":" + (this.arrayNoms[i].getAttribute("headers")) + "," +
        "\"artiste\":" + ((this.arrayVendeurs[i] as HTMLInputElement).checked) + "," +
        "\"compteBloque\":" + ((this.arrayBloques[i] as HTMLInputElement).checked) +
        "},";
    }
    this.json += "{" +
      "\"id\":" + (this.arrayNoms[this.arrayBloques.length - 1].getAttribute("headers")) + "," +
      "\"artiste\":" + ((this.arrayVendeurs[this.arrayBloques.length - 1] as HTMLInputElement).checked) + "," +
      "\"compteBloque\":" + ((this.arrayBloques[this.arrayBloques.length - 1] as HTMLInputElement).checked) +
      "}]";
    this._utilisateursService.postUpdateActivVendeur(this.json).subscribe(() => {
      this.onListeUtilisateurs();
    });
  }

  onSubmitStatutCommandes() {
    this.json = "[";
    for (let i = 0; i < this.arrayCommandes.length - 1; i++) {
      this.json += "{" +
        "\"id\":" + (this.arrayCommandesId[i].textContent) + "," +
        "\"statut\":";
      switch ((this.arrayCommandesStatuts[i] as HTMLSelectElement).options[(this.arrayCommandesStatuts[i] as HTMLSelectElement).selectedIndex].text) {
        case "En attente de traitement": { this.json += "EN_ATTENTE_DE_TRAITEMENT"; break; }
        case "En cours de préparation": { this.json += "EN_COURS_DE_PREPARATION"; break; }
        case "En cours d'acheminement": { this.json += "EN_COURS_D_ACHEMINEMENT"; break; }
        case "Livrée": { this.json += "LIVREE"; break; }
        default: { this.json += "..."; break; }
      }
      this.json += "},";
    }
    this.json += "{" +
      "\"id\":" + (this.arrayCommandesId[this.arrayCommandes.length - 1].textContent) + "," +
      "\"statut\":";
    switch ((this.arrayCommandesStatuts[this.arrayCommandes.length - 1] as HTMLSelectElement).options[(this.arrayCommandesStatuts[this.arrayCommandes.length - 1] as HTMLSelectElement).selectedIndex].text) {
      case "En attente de traitement": { this.json += "EN_ATTENTE_DE_TRAITEMENT"; break; }
      case "En cours de préparation": { this.json += "EN_COURS_DE_PREPARATION"; break; }
      case "En cours d'acheminement": { this.json += "EN_COURS_D_ACHEMINEMENT"; break; }
      case "Livrée": { this.json += "LIVREE"; break; }
      default: { this.json += "..."; break; }
    }
    this.json += "}]";
    this._commandesService.postStatutCommande(this.json).subscribe(() => {
      this.onListeCommandes();
      this.onListeCommandesParUtilisateur();
    });
  }

  onSubmitNouvelleImage() {
    this._oeuvresService.postNouvelleImage(this.selectedFile);
  }

  onSubmitNouvelleVente() {
    this.json = "{";
    this.json += "\"titrePhoto\":\"" + this.titre + "\"," +
      "\"nbTirages\":" + this.nb_tirages + "," +
      "\"prixDeBase\":" + this.tarif + "," +
      "\"formats\": [CLASSIQUE,";
    for (let i = 0; i < this.arrayFormatsChoisis.length; i++) {
      if ((this.arrayFormatsChoisis[i] as HTMLInputElement).checked) {
        this.json += "\"" + (this.arrayFormatsChoisis[i] as HTMLInputElement).value + "\",";
      }
    }
    this.json = this.json.substring(0, this.json.length - 1);
    this.json += "],";
    this.json += "\"themes\": [";
    let compteur = 0;
    for (let i = 0; i < this.arrayThemesChoisis.length; i++) {
      if ((this.arrayThemesChoisis[i] as HTMLInputElement).checked) {
        compteur = 1;
        this.json += "\"" + (this.arrayThemesChoisis[i] as HTMLInputElement).value + "\",";
      }
    }
    if (compteur == 1) {
      this.json = this.json.substring(0, this.json.length - 1);
    }
    this.json += "]";
    this.json += "}";
    this._oeuvresService.postNouvelleOeuvre(this._sessionSt.retrieve("user").id, this.json);
    this.titre = "";
    this.nb_tirages = null;
    this.tarif = null;
    for (let i = 0; i < this.arrayFormatsChoisis.length; i++) {
      (this.arrayFormatsChoisis[i] as HTMLInputElement).checked = false;
    }
    for (let i = 0; i < this.arrayThemesChoisis.length; i++) {
      (this.arrayThemesChoisis[i] as HTMLInputElement).checked = false
    }
  }

  onSubmitBiographie(){
    this.json = "{";
    this.json += "\"biographie\":\"" + this.biographieData + "\"}";

    this._utilisateursService.postBiographie(this._sessionSt.retrieve("user").id, this.json).subscribe(()=>{
      this._sessionSt.retrieve("user").biographie = this.biographieData;
      this.onUtilisateurSession();
    });
  }

  onUtilisateurSession() {
    this._utilisateursService.getUtilisateurSession(this._sessionSt.retrieve("user").id).subscribe(data => {
      this.sessionclient = data;
      this.admin = this._sessionSt.retrieve("user").admin;
      this.vendeur = this._sessionSt.retrieve("user").artiste;
      this.biographieData = this._sessionSt.retrieve("user").biographie;
      this._sessionSt.store("user", this._sessionSt.retrieve("user"));
    });
  }

  onListeUtilisateurs() {
    this._utilisateursService.getListeUtilisateurs().subscribe(data => {
      this.utilisateurs = data;
    });
  }

  onListeCommandes() {
    this._commandesService.getCommandes().subscribe(data => {
      this.commandesList = data;
    })
  }

  onListeCommandesParUtilisateur(){
    this._commandesService.getCommandesParUtilisateur(this._sessionSt.retrieve("user").id).subscribe(data => {
      this.commandesParUtilisateur = data;
      console.log(this.commandesParUtilisateur);
    })
  }

  onCommandeViaId(id: number) {
    this._commandesService.getCommandeViaId(id).subscribe(data => {
      this.commandes_id = data;
      this.oeuvresList_id = this.commandes_id.oeuvres;
    });
  }

  onListeCommandesCourantes() {
    this._oeuvresService.getOeuvresEnCoursParArtiste(this._sessionSt.retrieve("user").id).subscribe(data => {
      this.oeuvresCourantes = data;
    })
  }

  onListeCommandesPassees() {
    this._oeuvresService.getOeuvresPasseesParArtiste(this._sessionSt.retrieve("user").id).subscribe(data => {
      this.oeuvresPassees = data;
    })
  }


  onDonnees() {
    this.donnees = true;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onAdresse() {
    this.donnees = false;
    this.adresse = true;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onCommandes() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = true;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onNouvelle() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = true;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onEnCours() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = true;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onPassees() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = true;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onBiographie() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = true;
    this.activ_vendeurs = false;
    this.statut_commandes = false;
  }

  onActivVendeurs() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = true;
    this.statut_commandes = false;
  }

  onStatutCommandes() {
    this.donnees = false;
    this.adresse = false;
    this.commandes = false;
    this.nouvelle_vente = false;
    this.vente_en_cours = false;
    this.ventes_passees = false;
    this.biographie = false;
    this.activ_vendeurs = false;
    this.statut_commandes = true;
  }


}
