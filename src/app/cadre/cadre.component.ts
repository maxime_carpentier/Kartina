import { Component, OnInit, Output, EventEmitter, OnChanges, Input } from '@angular/core';
import { Cadre } from '../enum/Cadre';

@Component({
  selector: 'app-cadre',
  templateUrl: './cadre.component.html',
  styleUrls: ['./cadre.component.scss']
})
export class CadreComponent implements OnInit, OnChanges {

  private aluminium : boolean = true;
  private autres_finitions : boolean = false;
  
  private choixCadre:boolean = false;
  
  @Output() etapeAchat = new EventEmitter<boolean>();		
	@Output() cadreData = new EventEmitter<Cadre>();
  @Output() etapeFinition = new EventEmitter<boolean>();
  
  
  
  @Input() finitionChoisi:any;

  constructor() { }

  ngOnInit() {
  }

ngOnChanges()
  {
  }
  
  
  choixSans()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.SANS_ENCADREMENT);
    this.surlignementChoix("sans");
  }
  
  choixNoirSatin()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.NOIR_SATIN);
    this.surlignementChoix("noirsatin");
  }
  
  choixBlancSatin()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.BLANC_SATIN);
    this.surlignementChoix("blancsatin");
  }
  
  choixNoyer()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.NOYER);
    this.surlignementChoix("noyer");
  }
  
  choixChene()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.CHÊNE);
    this.surlignementChoix("chene");
  }
  
  choixAluNoir()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.ALUMINIUM_NOIR);
    this.surlignementChoix("alunoir");
  }
  
  choixBoisBlanc()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.BOIS_BLANC);
    this.surlignementChoix("boisblanc");
  }
  
  choixAcajouMat()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.ACAJOU_MAT);
    this.surlignementChoix("acajoumat");
  }
  
  choixAluBrosse()
  {
  	this.choixCadre = true;
    this.cadreData.emit(Cadre.ALUMINIUM_BROSSE);
    this.surlignementChoix("alubrosse");
  }
  
  surlignementChoix(choix: string) {
    let choixPossibles: Array<string>;
    if (this.finitionChoisi==1 || this.finitionChoisi==0){
      choixPossibles = ["sans", "noirsatin", "blancsatin", "noyer", "chene"];
    } else if (this.finitionChoisi==3 || this.finitionChoisi==4){
      choixPossibles = ["alunoir", "boisblanc", "acajoumat", "alubrosse"];
    }
    for (let i = 0; i < choixPossibles.length; i++) {
      (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseenter = function () {
      };
      (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseleave = function () {
      };
      if (choixPossibles[i] == choix) {
        console.log("if", choixPossibles[i]);
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#544a3f";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "white";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "none";
      } else {
        console.log("else", choixPossibles[i]);
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#f9fbff";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "#544a3f";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "2px solid #a89683";
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseenter = function () {
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#544a3f";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "white";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "none";
        };
        (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).onmouseleave = function () {
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.backgroundColor = "#f9fbff";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.color = "#544a3f";
          (document.getElementsByClassName(choixPossibles[i])[0] as HTMLElement).style.border = "2px solid #a89683";
        };
      }
    }
  }

  
  versAchat()
  {
    this.etapeAchat.emit(true);
  }
  
  versFinition()
  {
  	this.etapeFinition.emit(true);
  }
  
  
}
