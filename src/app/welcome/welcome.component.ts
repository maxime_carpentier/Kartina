import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Orientation } from '../enum/Orientation';
import { Router } from '@angular/router';

@Component({
	selector: 'app-welcome',
	templateUrl: './welcome.component.html',
	styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {


	private photos: any;

	private photo0: any;
	private photo1: any;
	private photo2: any;
	private photo3: any;
	private photo4: any;
	private photo5: any;
	private photo6: any;
	private photo7: any;
	private photo8: any;
	private photo9: any;
	private photo10: any;

	private titre1: string;
	private titre2: string;
	private titre3: string;
	private titre4: string;
	private titre5: string;
	private titre6: string;

	private artiste1: string;
	private artiste2: string;
	private artiste3: string;
	private artiste4: string;
	private artiste5: string;
	private artiste6: string;
	private artiste9: string;

	private prix1: any;
	private prix2: any;
	private prix3: any;
	private prix4: any;
	private prix5: any;
	private prix6: any;

	private id1:any;
	private id2:any;
	private id3:any;
	private id4:any;
	private id5:any;
	private id6:any;

	private idartiste1: any;
	private idartiste2: any;
	private idartiste3: any;
	private idartiste4: any;
	private idartiste5: any;
	private idartiste6: any;
	private idartiste9: any;

	private orientation0: string;
	private orientation1: string;
	private orientation2: string;
	private orientation3: string;
	private orientation4: string;
	private orientation5: string;
	private orientation6: string;

	private arrayPhotos: Array<any> = new Array<any>();

	constructor(public http: HttpClient, private _router: Router) {
		console.log('Le constructor est appelé');

		this.http.get("http://localhost:8080/Kartina/AccueilTest").subscribe(data => {
			this.photos = data;
			console.log(this.photos);
			console.log(this.photos[0].lienImage);
			this.photo0 = this.photos[0].lienImage;
			this.photo1 = this.photos[1].lienImage;
			this.photo2 = this.photos[2].lienImage;
			this.photo3 = this.photos[3].lienImage;
			this.photo4 = this.photos[4].lienImage;
			this.photo5 = this.photos[5].lienImage;
			this.photo6 = this.photos[6].lienImage;
			this.photo7 = this.photos[7].lienImage;
			this.photo8 = this.photos[8].lienImage;
			this.photo9 = this.photos[9].lienImage;
			this.photo10 = this.photos[10].lienImage;

			this.id1 = this.photos[1].id;
			this.id2 = this.photos[2].id;
			this.id3 = this.photos[3].id;
			this.id4 = this.photos[4].id;
			this.id5 = this.photos[5].id;
			this.id6 = this.photos[6].id;

			this.orientation0 = this.photos[0].orientation;
			this.orientation1 = this.photos[1].orientation;
			this.orientation2 = this.photos[2].orientation;
			this.orientation3 = this.photos[3].orientation;
			this.orientation4 = this.photos[4].orientation;
			this.orientation5 = this.photos[5].orientation;
			this.orientation6 = this.photos[6].orientation;

			this.titre1 = this.photos[1].titrePhoto;
			this.titre2 = this.photos[2].titrePhoto;
			this.titre3 = this.photos[3].titrePhoto;
			this.titre4 = this.photos[4].titrePhoto;
			this.titre5 = this.photos[5].titrePhoto;
			this.titre6 = this.photos[6].titrePhoto;

			this.artiste1 = this.photos[1].artiste.prenom + " " + this.photos[1].artiste.nom;
			this.artiste2 = this.photos[2].artiste.prenom + " " + this.photos[2].artiste.nom;
			this.artiste3 = this.photos[3].artiste.prenom + " " + this.photos[3].artiste.nom;
			this.artiste4 = this.photos[4].artiste.prenom + " " + this.photos[4].artiste.nom;
			this.artiste5 = this.photos[5].artiste.prenom + " " + this.photos[5].artiste.nom;
			this.artiste6 = this.photos[6].artiste.prenom + " " + this.photos[6].artiste.nom;
			this.artiste9 = this.photos[9].artiste.prenom + " " + this.photos[9].artiste.nom;

			this.idartiste1 = this.photos[1].artiste.id;
			this.idartiste2 = this.photos[2].artiste.id;
			this.idartiste3 = this.photos[3].artiste.id;
			this.idartiste4 = this.photos[4].artiste.id;
			this.idartiste5 = this.photos[5].artiste.id;
			this.idartiste6 = this.photos[6].artiste.id;
			this.idartiste9 = this.photos[9].artiste.id;

			this.prix1 = this.photos[1].prixDeBase;
			this.prix2 = this.photos[2].prixDeBase;
			this.prix3 = this.photos[3].prixDeBase;
			this.prix4 = this.photos[4].prixDeBase;
			this.prix5 = this.photos[5].prixDeBase;
			this.prix6 = this.photos[6].prixDeBase;

			(document.getElementById("quatro1") as HTMLElement).style.backgroundImage = "url(\"http://localhost:8080/Kartina/photos/" + this.photo7 + "\")";
			document.getElementById("quatro2").style.backgroundImage = "url(\"http://localhost:8080/Kartina/photos/" + this.photo8 + "\")";
			document.getElementById("quatro3").style.backgroundImage = "url(\"http://localhost:8080/Kartina/photos/" + this.photo9 + "\")";
			document.getElementById("quatro4").style.backgroundImage = "url(\"http://localhost:8080/Kartina/photos/" + this.photo10 + "\")";


		});
	}

	ngOnInit() {
		console.log('Le composant a fini son initialisation');
		//this.http.get("http://localhost:8080/Kartina/AccueilTest").subscribe();
	}

	ngAfterViewInit() {
		window.scroll(0, 0);
	}

	routingToAchat(id: number) {
        this._router.navigate(['/achat', id]);
    }

}
