import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Ng2Webstorage } from 'ngx-webstorage';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { ErrorComponent } from './error/error.component';
import { PhotographiesComponent } from './photographies/photographies.component';
import { FilarianeComponent } from './filariane/filariane.component';
import { AideComponent } from './aide/aide.component';
import { ArtisteComponent } from './artiste/artiste.component';
import { ArtistesComponent } from './artistes/artistes.component';
import { PanierComponent } from './panier/panier.component';
import { AchatComponent } from './achat/achat.component';
import { FormatComponent } from './format/format.component';
import { FinitionComponent } from './finition/finition.component';
import { CadreComponent } from './cadre/cadre.component';
import { PaiementComponent } from './paiement/paiement.component';
import { CompteComponent } from './compte/compte.component';
import { CreercompteComponent } from './creercompte/creercompte.component';
import { UserComponent } from './user/user.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { HttpClientModule } from "@angular/common/http";
import { ConditionsComponent } from './conditions/conditions.component';
import { MentionsComponent } from './mentions/mentions.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    WelcomeComponent,
    ErrorComponent,
    PhotographiesComponent,
    FilarianeComponent,
    AideComponent,
    ArtisteComponent,
    ArtistesComponent,
    PanierComponent,
    AchatComponent,
    FormatComponent,
    FinitionComponent,
    CadreComponent,
    PaiementComponent,
    CompteComponent,
    CreercompteComponent,
    UserComponent,
    ConditionsComponent,
    MentionsComponent
  ],
  imports: [
    HttpClientModule,
    Ng2Webstorage,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    RouterModule.forRoot([
      {path: "welcome", component: WelcomeComponent},
      {path: 'photographies', component: PhotographiesComponent},
      {path: 'photographies/:neworlast', component: PhotographiesComponent},
      {path: 'aide', component: AideComponent},
      {path: 'artistes', component: ArtistesComponent},
      {path: 'artiste/:idArtiste', component: ArtisteComponent},
      {path: 'panier', component: PanierComponent},
      {path: 'achat/:idPhoto', component: AchatComponent},
      {path: 'paiement', component: PaiementComponent},
      {path: 'compte', component: CompteComponent},
      {path: 'creercompte', component: CreercompteComponent},
      {path: 'mentions', component: MentionsComponent},
      {path: 'conditions', component: ConditionsComponent},
      {path: 'user', component: UserComponent},
      {path: '', component: WelcomeComponent},
      {path: '**', component: ErrorComponent}
      
    ],{

      scrollPositionRestoration: 'enabled',

      anchorScrolling: 'enabled',

    })
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
